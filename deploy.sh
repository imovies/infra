#!/bin/sh
MACHINES=(frontend backend ca-generator database)

if test $# -gt 0; then
	MACHINES=("$@")
fi

for machine in "${MACHINES[@]}"; do
	NIX_SSHOPTS="-t" \
	nixos-rebuild switch --use-remote-sudo --flake ".#$machine" --target-host $machine
done

