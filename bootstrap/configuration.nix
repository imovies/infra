let
  self =
    builtins.getFlake "gitlab:imovies/infra?host=gitlab.ethz.ch";
in
  {config, ...}: {
    imports = [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix

      "${self}/modules/core"
    ];

    # Use the GRUB 2 boot loader.
    boot.loader.grub = {
      enable = true;
      version = 2;
      efiSupport = true;
      efiInstallAsRemovable = true;
      device = "nodev";
    };
    boot.loader.efi.efiSysMountPoint = "/boot/efi";

    system.stateVersion = "22.05";
  }
