{
  config,
  lib,
  ...
}: {
  lib.imovies.intranet = lib.importTOML ./data.toml;

  networking.hosts = lib.mapAttrs' (name: m: lib.nameValuePair m.ipv4 [name]) config.lib.imovies.intranet.machines;
}
