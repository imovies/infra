{
  config,
  lib,
  ...
}: {
  lib.imovies.backend-backup = lib.importTOML ./data.toml;

  networking.hosts = lib.mapAttrs' (name: m: lib.nameValuePair m.ipv4 [name]) config.lib.imovies.backend-backup.machines;
}
