{
  imports = [
    ./system-administrators.nix
    ./privilege-escalation.nix
  ];

  nix.settings.trusted-public-keys = [
    "doenut:69sPWntXFKFHo3v8c1BUZgdltjRq9UKlrpGlcNmbXvE="
  ];
}
