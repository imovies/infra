{
  users.users.jane = {
    isNormalUser = true;
    extraGroups = ["wheel"]; # Enable ‘sudo’ for the user.
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBWpPIOSVHTSEqz7j64ffRZyswYY8KymnI5rjjZ3a9AK jane@doenut"
    ];
  };
}
