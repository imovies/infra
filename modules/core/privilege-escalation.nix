{lib, ...}: {
  security.sudo.execWheelOnly = true;

  security.wrappers.su.group = lib.mkForce "wheel";
  security.wrappers.su.permissions = "u+rx,g+x";

  # ask sudoers for the root password
  security.sudo.wheelNeedsPassword = true;
  security.sudo.extraConfig = ''
    Defaults rootpw
  '';
}
