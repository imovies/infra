# The flake file is the entry point for nix commands
{
  description = "iMovies infrastructure-as-code repository";

  # Inputs are how Nix can use code from outside the flake during evaluation.
  inputs.ca-generator-app.url = "gitlab:imovies/ca_generator_app?host=gitlab.ethz.ch";
  inputs.ca-generator-app.inputs.nixpkgs.follows = "nixos-stable";
  inputs.frontend-app.url = "gitlab:imovies/frontend?host=gitlab.ethz.ch";
  inputs.frontend-app.inputs.nixpkgs.follows = "nixos-stable";
  inputs.backend-app.url = "gitlab:imovies/imovies-backend?host=gitlab.ethz.ch";
  inputs.backend-app.inputs.nixpkgs.follows = "nixos-stable";
  inputs.fup.url = "github:gytis-ivaskevicius/flake-utils-plus/v1.3.1";
  inputs.nixos-stable.url = "nixpkgs/nixos-22.05";

  # Outputs are the public-facing interface to the flake.
  outputs = inputs @ {
    self,
    fup,
    ...
  }:
    fup.lib.mkFlake {
      inherit self inputs;

      hostDefaults = {
        system = "x86_64-linux";
        channelName = "nixos-stable";
        specialArgs = {
          inherit self inputs;
        };
      };

      hosts = let
        dir = ./machines;
      in
        builtins.mapAttrs (name: _: {
          modules = [(dir + "/${name}/configuration.nix")];
        }) (builtins.readDir dir);

      outputsBuilder = channels: {
        formatter = channels.nixos-stable.alejandra;
      };
    };
}
