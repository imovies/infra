# NOTE: due to the fact that this machine is not remotely accessible,
# configuration drift is expected.
{
  config,
  pkgs,
  self,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix

    "${self}/modules/core"
    "${self}/modules/frontend-backup/backup-machine"
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub = {
    enable = true;
    version = 2;
    efiSupport = true;
    efiInstallAsRemovable = true;
    device = "nodev";
  };
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  environment.systemPackages = with pkgs; [git];

  networking.interfaces.enp0s3.ipv4.addresses = [
    {
      address = "${config.lib.imovies.frontend-backup.machines.${config.networking.hostName}.ipv4}";
      prefixLength = 24;
    }
  ];

  system.stateVersion = "22.05";
}
