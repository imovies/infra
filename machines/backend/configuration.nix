{
  config,
  inputs,
  pkgs,
  self,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix

    "${self}/modules/core"
    "${self}/modules/remote-access"
    "${self}/modules/intranet/endsystem"
    "${self}/modules/backend-backup/target-machine"
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub = {
    enable = true;
    version = 2;
    efiSupport = true;
    efiInstallAsRemovable = true;
    device = "nodev";
  };
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  services.httpd = {
    enable = true;
    adminAddr = "webmaster@imovies.ch";
    extraModules = ["proxy_uwsgi"];
    virtualHosts."app".locations."/".proxyPass = "unix:/run/uwsgi/uwsgi.sock|uwsgi://%{HTTP_HOST}/";
  };
  services.uwsgi = {
    enable = true;
    group = "wwwrun";
    plugins = ["python3"];
    instance.type = "normal";
    instance.pythonPackages = _: [inputs.backend-app.defaultPackage.${pkgs.system}];
    instance.module = "application:app";
    instance.socket = "/run/uwsgi/uwsgi.sock";
    instance.chmod-socket = "660";
    instance.logto = "/var/log/uwsgi/backend_app.log";
  };

  systemd.tmpfiles.rules = [
    "d /var/log/uwsgi 700 uwsgi"
    "d /var/lib/ca_generator_app 755 uwsgi"
  ];

  networking.firewall.allowedTCPPorts = [80 443];

  # disaster recovery keys for backup encryption.
  # we support multiple keys, one per line.
  environment.etc."imovies/recovery-keys.key".text = ''
    age1ps9ex9g54dda920eexguw3f7dxglkhhyy9r0uhetyrj9e6qrc5nsdq0e4e
  '';

  environment.systemPackages = with pkgs; [age];

  networking.interfaces.enp0s8.ipv4.addresses = [
    {
      address = "${config.lib.imovies.backend-backup.machines.${config.networking.hostName}.ipv4}";
      prefixLength = 24;
    }
  ];

  system.stateVersion = "22.05";
}
