{
  config,
  pkgs,
  self,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix

    "${self}/modules/core"
    "${self}/modules/remote-access"
    "${self}/modules/intranet/endsystem"
    "${self}/modules/backend-backup/target-machine"
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub = {
    enable = true;
    version = 2;
    efiSupport = true;
    efiInstallAsRemovable = true;
    device = "nodev";
  };
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
  };

  services.mysqlBackup = {
    enable = true;
    databases = [ "imovies" ];
    # keep under /var/log for backup machine access
    location = "/var/log/database-backup/";
  };

  networking.firewall.allowedTCPPorts = [3306];

  networking.interfaces.enp0s8.ipv4.addresses = [
    {
      address = "${config.lib.imovies.backend-backup.machines.${config.networking.hostName}.ipv4}";
      prefixLength = 24;
    }
  ];

  system.stateVersion = "22.05";
}
